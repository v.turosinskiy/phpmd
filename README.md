<<<<<<< HEAD
# SwaggerMD

SwaggerMD - библиотека для генерации markdown документации на основе swagger.json файла
=======
# Генератор документации
>>>>>>> cc47a571015a6398203034ada379f3ddb7fbf441

## Используемые компоненты
Для парсинга аннотаций используется библиотека [zircote/swagger-php](https://github.com/zircote/swagger-php) которая поддерживает стандарт [OpenAPI Specification 2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md)

## Установка
```bash
cd doc-generator
cp .env.example .env
composer install --dev
```

## Настройка
Файл с настройками: [.env](./.env)
Пример файла с настройками: [.env.example](./.env.example)

Файл настроек содержит следующие параметры:

- `SWG_JSON` - Директория в которую будет сохранен json файл по стандарту Open API 2
- `SWG_BUILD` - Директория куда будут сохранены файлы Markdown документации
- `SWG_SCAN` - Директории из которых будет собираться информация для генерации документации

## Список команд

1. Список команд
    ```bash
    make help
    ```
2. Установка зависимостей и создание файла конфигураций
    ```bash
    make swg-build
    ```
3. Генерация документации
    ```bash
    make swg-docs
    ```
4. Удаление файлов документации
    ```bash
    make swg-clean
    ``` 

## Быстрый старт
<<<<<<< HEAD
1. [Структура проекта](./docs/quick-start/Readme.md)
2. [Общее описание сервиса](./docs/quick-start/Swagger.md)
3. [Описание модели](./docs/quick-start/Model.md)
4. [Описание метода](./docs/quick-start/Action.md)
5. [Особенности генерации Markdown документации](./docs/quick-start/Markdown.md)

## Расширенная документация по классам
1. [@SWG\Contact](./docs/write_annotations/Contact.md) - Контактных данных
2. [@SWG\Definition](./docs/write_annotations/Definition.md) - Константы
3. [@SWG\Delete](./docs/write_annotations/Tag.md) - Запрос Delete
4. [@SWG\ExternalDocumentation](./docs/write_annotations/ExternalDocumentation.md) - Ссылка на подробную документацию
5. [@SWG\Get](./docs/write_annotations/Get.md) - Запрос Get
6. [@SWG\Head](./docs/write_annotations/Head.md) - Запрос Head
7. [@SWG\Header](./docs/write_annotations/Header.md) - Заголовок
8. [@SWG\Info](./docs/write_annotations/Info.md) - Информация о проекте
9. [@SWG\Items](./docs/write_annotations/Items.md) - Элемент массива
10. [@SWG\License](./docs/write_annotations/License.md) - Используемой лицензии
11. [@SWG\Operation](./docs/write_annotations/Operation.md) - Операции
12. [@SWG\Options](./docs/write_annotations/Options.md) - Опции запроса
13. [@SWG\Parameter](./docs/write_annotations/Parameter.md) - Параметр запроса
14. [@SWG\Patch](./docs/write_annotations/Patch.md) - Запроса Patch
15. [@SWG\Path](./docs/write_annotations/Path.md) - Путь запроса
16. [@SWG\Post](./docs/write_annotations/Post.md) - Запрос Post
17. [@SWG\Property](./docs/write_annotations/Property.md) - Свойство
18. [@SWG\Put](./docs/write_annotations/Put.md) - Запрос Put
19. [@SWG\Response](./docs/write_annotations/Response.md) - Ответ на запрос
20. [@SWG\Schema](./docs/write_annotations/Schema.md) - Схема обьекта
21. [@SWG\SecuritySchema](./docs/write_annotations/SecuritySchema.md) - Схемы безопасности для авторизации
22. [@SWG\Swagger](./docs/write_annotations/Swagger.md) - Данные о Swagger
23. [@SWG\Tag](./docs/write_annotations/Tag.md) - Сущность
24. [@SWG\XML](./docs/write_annotations/XML.md) - XML обьект
=======
1. [Структура проекта](./src/quick-start/Readme.md)
2. [Общее описание сервиса](./src/quick-start/Swagger.md)
3. [Описание модели](./src/quick-start/Model.md)
4. [Описание метода](./src/quick-start/Action.md)
5. [Особенности генерации Markdown документации](./src/quick-start/Markdown.md)

## Расширенная документация по классам
1. [@SWG\Contact](./src/write_annotations/Contact.md) - Контактных данных
2. [@SWG\Definition](./src/write_annotations/Definition.md) - Константы
3. [@SWG\Delete](./src/write_annotations/Tag.md) - Запрос Delete
4. [@SWG\ExternalDocumentation](./src/write_annotations/ExternalDocumentation.md) - Ссылка на подробную документацию
5. [@SWG\Get](./src/write_annotations/Get.md) - Запрос Get
6. [@SWG\Head](./src/write_annotations/Head.md) - Запрос Head
7. [@SWG\Header](./src/write_annotations/Header.md) - Заголовок
8. [@SWG\Info](./src/write_annotations/Info.md) - Информация о проекте
9. [@SWG\Items](./src/write_annotations/Items.md) - Элемент массива
10. [@SWG\License](./src/write_annotations/License.md) - Используемой лицензии
11. [@SWG\Operation](./src/write_annotations/Operation.md) - Операции
12. [@SWG\Options](./src/write_annotations/Options.md) - Опции запроса
13. [@SWG\Parameter](./src/write_annotations/Parameter.md) - Параметр запроса
14. [@SWG\Patch](./src/write_annotations/Patch.md) - Запроса Patch
15. [@SWG\Path](./src/write_annotations/Path.md) - Путь запроса
16. [@SWG\Post](./src/write_annotations/Post.md) - Запрос Post
17. [@SWG\Property](./src/write_annotations/Property.md) - Свойство
18. [@SWG\Put](./src/write_annotations/Put.md) - Запрос Put
19. [@SWG\Response](./src/write_annotations/Response.md) - Ответ на запрос
20. [@SWG\Schema](./src/write_annotations/Schema.md) - Схема обьекта
21. [@SWG\SecuritySchema](./src/write_annotations/SecuritySchema.md) - Схемы безопасности для авторизации
22. [@SWG\Swagger](./src/write_annotations/Swagger.md) - Данные о Swagger
23. [@SWG\Tag](./src/write_annotations/Tag.md) - Сущность
24. [@SWG\XML](./src/write_annotations/XML.md) - XML обьект
>>>>>>> cc47a571015a6398203034ada379f3ddb7fbf441
