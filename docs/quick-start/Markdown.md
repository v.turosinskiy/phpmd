# Особенности генерации Markdown документации

Оглавление:  
1. [Дополнительные атрибуты](#additional)
2. [Использование снипетов](#snippets)
2. [Использование ссылок](#links)
<a name="additional"></a>
## Дополнительные атрибуты
Markdown документация формируется на основе swagger.json файла.

Open API спецификация содержит ограниченный набор атрибутов для описания сервисов. Для их расширения используется атрибут `x-somefield`, который позволяет расширениям хранить необходимые данные в этих полях.

Список параметро вна которые стоит обратить внимание:
1. @SWG\Property
  - `nullable` - свойство может принимать значение null
  - `filtrable` - свойство подлежит фильтрации
  - `sortable` - свойство подлежит сортировке
  - `deprecated` - свойство помечено как Deprecated и не рекомендуется для использования
  - `post` - описание свойства при использовании его в качестве `POST` параметра 
2. @SWG\Definition
  - `deprecated` - Модель помечена как Deprecated и не рекомендуется для использования
  - `namespace` - вложенность моделей на странице списка моделей
3. @SWG\Action
  - `filter` - прямое указание на список свойств, которые подлежат фильтрации
  - `sort` - прямое указание на список свойств, которые подлежат сортировке

<a name="snippets"></a>
## Использование снипетов
Пример файла snippet.json:

```json
{
  "text_for_replace": {
    "type": "replace",
    "content": "Hello world"
  },
  "another_text_for_replace":{
    "type": "replace",
    "content":"..."
  }
}
```
Где:
1. `ключ` - это название функций которое подставляется после `swg_snippet.`
2. `type` - тип действия.
### Вставка текста

Пример:
```json
{
  "text_for_replace": {
    "type": "replace",
    "content": "Hello world"
  }
}
```
  - `content` - текст на который заменить текст вызова снипета  

Пример добавления сниппета в аннотации:
```php
/**
 * @SWG\Get(
 *     ...
 *     description="<!--[%swg_snippet.text_for_replace()%]-->",
 *     ...
 */
```
<a name="links"></a>
## Использование ссылок
Для того, чтобы избежать дублирования параметров и свойств рекомендуется использовать при описании ссылки.

По умолчанию все свойства хранятся в файле `/annotations/properties.php`
Все параметры храняться в файле `/annotations/parameters.php`

Примеры использования ссылок:
1. Ссылка на параметры
    ```php
    * @SWG\Parameter(
    *     name="tags",
    *     ref="#/parameters/tags"
    * ),
    ```
2. Ссылка на параметры
    ```php
    * @SWG\Property(
    *     property="lang",
    *     ref="#/definitions/CommonProperties/properties/lang"
    * ),
    ```
3. Ссылка на модели
    ```php
    * @SWG\Definition(definition="AttachPublic", title="Публичные приложенные файлы", type="object", @SWG\Xml(name="AttachPublic"),
    *     x={"namespace"="Приложенные файлы"},
    *     ref="$/definitions/Attach"
    * )
    ```
    Необходимо обратить внимание ссылка формируется чере знак `$`, а не '#'.

4. Ссылка на свойства модели
    ```php
    * @SWG\Property(
    *     property="id",
    *     ref="#/definitions/Attach/properties/id"
    * ),
    ```
5. Привязка модели в виде свойства к другой модели или в схеме как возвращаемое значение
    ```php
    * @SWG\Property(
    *     property="attach",
    *     ref="#/definitions/Attach"
    * ),
    ```

[К Оглавлению](./../../README.md)