# Описание метода
Содержание:
1. Тип
    - [@SWG\Get](../annotations/Get.md)
    - [@SWG\Post](../annotations/Post.md)
    - ...
2. Путь вызова `path="`
3. Название и описание `summary` и `description`
4. Тэги `tags`
5. Тип ответа `produces`
4. Список обязательных и необязательных параметров [@SWG\Parameter](../annotations/Parameter.md)
5. Список ответов [@SWG\Response](../annotations/Response.md)
  - код ответа и сообщение 
  - схема ответа - ([@SWG\Schema](../annotations/Schema.md))
6. Если требуется авторизация, то дополнительно нужно указать тип авторизации `security`
Пример:

```php
/**
 * @SWG\Get(
 *     path="/api/example",
 *     summary="Примеры моделей: Список",
 *     tags={"Примеры моделей"},
 *     description="Возвращается информация о доступных примерах моделей",
 *     produces={"application/json"},
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *         @SWG\Schema(
 *             @SWG\Property(
 *                 type="array",
 *                 property="items",
 *                 @SWG\Items(
 *                     ref="#/definitions/Example"
 *                 )
 *             )
 *         )
 *     ),
 *     security={{"api_key": {}}},
 * )
 */
```
[Подробнее...](../write_annotations/Operation.md)

[К Оглавлению](./../../README.md)