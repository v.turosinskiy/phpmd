# Общее описание сервиса

Пример:
```php
/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="localhost",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Заголовок",
 *         description="Краткое описание"
 *   ),
 * )
 */
```
[Подробнее...](../write_annotations/Swagger.md)

[К Оглавлению](./../../README.md)