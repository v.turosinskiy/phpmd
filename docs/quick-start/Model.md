# Описание модели
Содержание:
1. Название `title`
2. Уникальный код `definition` и `name`
3. Тип `type`
4. Положение в оглавлении на странице моделей `namespace`
5. Свойства [@SWG\Property](../write_annotations/Property.md)

Пример:

```php
/**
 * @SWG\Definition(definition="Article", title="Статья", type="object", @SWG\Xml(name="Article"),
 *     required={"title", "full_text", "article_type", "version", "published_at", "importance"},
 *     x={"namespace"="Статьи"},
 *     @SWG\Property(
 *         property="id",
 *         ref="#/definitions/CommonProperties/properties/id",
 *         example=5272073,
 *     ),
 *     @SWG\Property(
 *         property="lang",
 *         ref="#/definitions/CommonProperties/properties/lang"
 *     ),
 * )
 */
```

[К Оглавлению](./../../README.md)