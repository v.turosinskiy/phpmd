# @SWG\Parameter

## Описание
Класс [Swagger\Annotations\Parameter](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Parameter.php)

## Особенности
1. Многократно используемые параметры должны быть расположены в файле `parameters.php`
2. В [@SWG\Response](./Response.md) в параметрах хранятся только ссылки
3. При необходимости атрибуты могут быть переопределены на уровне ответа
4. Именование параметров строится следующим образом:
    - Необязательный передается в query: `id`
    - Обязательный передается в query: `id_required`
    - Необязательный передается в path `id_path`
    - Обязательный передается в path `id_path_required`
5. Имена параметров могут переопределяться на уровне документации, но не могут на уровне Swagger UI

Пример:
```php
 * @SWG\Parameter(
 *     property="id",
 *     ref="#/parameters/id",
 *     name="Идентификатор статьи",
 *     description="Идентификатор Статьи",
 * )
```

## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации
- `ref`
- `parameter`
- `name`
- `in`
- `description`
- `required`
- `schema`
- `type`
- `format`
- `allowEmptyValue`
- `items`
    - обязателен если свойство типа `array`
- `collectionFormat` - формат поля используется если тип параметра является массивом
- `default`
- `maximum`
- `exclusiveMaximum`
- `minimum`
- `exclusiveMinimum`
- `maxLength`
- `minLength`
- `pattern`
- `maxItems`
- `minItems`
- `uniqueItems`
- `enum`
- `multipleOf`

## Примеры
Полное описание параметра:
```php
 * @SWG\Parameter(
 *     name="lang",
 *     description="Язык",
 *     in="path",
 *     required=true,
 *     type="string",
 *     enum={"ru", "en"},
 *     default="ru"
 * ),
```

Использование ранее обьявленного параметра:
```php
 * @SWG\Parameter(
 *     ref="#/parameters/lang"
 * )
```

[К Оглавлению](./../../README.md)