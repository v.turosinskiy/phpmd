# @SWG\Swagger

## Описание
Класс [Swagger\Annotations\Swagger](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Swagger.php)

## Примеры

```php
/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     host="petstore.swagger.io",
 *     schemes={"http"},
 *     produces={"application/json"},
 *     consumes={"application/json"}
 * )
 */
```

[К Оглавлению](./../../README.md)