# @SWG\Definition

## Описание

Класс: [Swagger\Annotations\Definition](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Definition.php)

OpenAPI Specification 2: [Definitions](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#definitions)

## Родительские аннотации
- [@SWG\Swagger](Swagger.md)

## Дочерние аннотации
- [@SWG\Property](Property.md)

## Свойства аннотации
- `definition` (string) - уникальное название модели
- `x` (object) - скрытое описание модели
    - не распознается Swagger UI
    - Используется для построения Markdown документации
        - `namespace`
            - Позволяет группировать модели в качестве разделителя выступает знак `/`
            ```json
            {"namespace":"Пресс-центр/Презентации"}
            ```
        - `deprecated` - Пометка, что модель `Deprecated`
            - Если передан как строка, то в документации будет дополнительно указано почему помечен как `Deprecated`
            ```json
            {"deprecated":true}
            ```
            ```json
            {"namespace":"Пресс-центр/Презентации"}
            ```
- [@SWG\Xml](./XML.md) - XML описание должно совпадать с definition
- `title` - Название
- `description` - Описнаие
- `ref` - Если указан, то все свойства родительской модели будут добавлены в текущую модель
    ```php
    /**
     * @SWG\Definition(
     * ...
     * ref="$/definitions/Article"
     * ...
     * )
    ```
## Примеры

1. Обьявление структуры класса в виде обьекта
    ```php
     /*
     * @SWG\Definition(
     *     definition="Article",
     *     title="Статья",
     *     type="object", 
     *     @SWG\Xml(
     *         name="Article"
     *     ),
     *     @SWG\Property(
     *         property="id",
     *         type="integer",
     *         description="Уникальный идентификатор",
     *         example=1,
     *     ),
     *     @SWG\Property(
     *         property="title",
     *         type="integer",
     *         description="Заголовок",
     *         example="Росстат: В июне инфляция в Москве составила 0,4%",
     *     ),
     * )
     */
    ```

2. Передача массива моделей `Article` с помощью [@SWG\Items](Items.md)
    ```php
    /**
     * @SWG\Items(
     *     ref="#/definitions/Article"
     * )
     */
    ```
3. Передача массива однотипных свойств `Article.title` с помощью [@SWG\Items](Items.md):
    ```php
    /**
     * @SWG\Items(
     *     ref="#/definitions/Article/title"
     * )
     */
    ```
4. Описание структуры ответа для использования в аннотациях [@SWG\Response](Response.md)
    ```php
     * @SWG\Definition(
     *     definition="ErrorModel",
     *     type="object",
     *     required={"code", "message"},
     *     @SWG\Property(
     *         property="code",
     *         type="integer",
     *     ),
     *     @SWG\Property(
     *         property="message",
     *         type="string"
     *     )
     * )
    ```
    
[К Оглавлению](./../../README.md)