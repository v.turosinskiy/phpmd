# @SWG\Property

## Описание
Класс [Swagger\Annotations\Property](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Property.php)

## Особенности
1. Многократно используемые свойства должны быть расположены в [@SWG\Definition](./Definition.md) `CommonProperties`
2. В свойствах у моделей храняться только ссылки
3. При необходимости атрибуты могут быть переопределены на уровне модели
4. Аттрибут `description` представляет из себя `Название` и `Описание` свойства
    - `Название` должно быть коротким и не содержать информацию о формате поле.
    - `Описание` отображается под таблицей как доп.описание поля. Содержит подробную информацию о формате поля, его назначении и другой информации.

Пример:
```php
 * @SWG\Property(
 *     property="id",
 *     ref="#/definitions/CommonProperties/properties/id",
 *     description="Идентификатор Статьи
Описание свойства",
 * )
```

## Базовые типы данных
1. boolean
    в json отображается как `true` или `false`
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     type="boolean",
     *     example=true,
     * ),
    ```
    Для отображения чисел 0 и 1 рекомендуется вместо него использовать искуственный тип `boolean`
2. integer
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     type="integer",
     *     example=11073,
     * ),
    ```
3. string
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     type="string",
     *     example="Пример заголовка",
     * )
    ```
4. enum string/enum integer
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     type="string",
     *     enum={"news", "interview", "article"}
     *     example="news",
     * )
    ```
5. array
     ```php
      * @SWG\Property(
     *     property="property_title",
      *     type="array",
      *     @Items(
      *         type="id"
      *     )
      * )
     ```
     [Подробнее о @SWG\Items](./Items.md)
6. date
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     type="date",
     *     example="news",
     * )
    ```
    Рекомендуется вместо него использовать искуственный тип `dateDefault`
7. dateTime
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     type="dateTime",
     *     example="news",
     * )
    ```
    Рекомендуется вместо него использовать искуственный тип `dateTimeDefault`
    
## Искуственные типы данных
1. boolean
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/boolean",
     *     example=1,
     * )
    ```
    
2. dateDefault
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/dateDefault",
     *     example="2017-10-24",
     * )
    ```
    Дата по формату: `yyyy-mm-dd`
3. datetimeDefault
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/datetimeDefault",
     *     example="27.05.2016 10:00:00",
     * )
    ```
    Дата и время по формату: `yyyy-mm-dd HH:ii:ss`
4. datetimeMaterial
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/datetimeMaterial",
     *     example=-62169993079,
     * )
    ```
    Дата и время по формату: `dd.mm.yyyy HH:ii:ss`
5. timestamp
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/timestamp",
     *     example=1464324480,
     * )
    ```
6. timestampString
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/timestampString",
     *     example="1464324480",
     * )
    ```
    Используется в том случае если timestamp передается как строка
7. timestampNegative
    ```php
     * @SWG\Property(
     *     property="property_title",
     *     ref="#/definitions/CommonTypes/properties/timestampNegative",
     *     example=-62169993079,
     * )
    ```
    Используется если `timestamp` передается как отрицательное число
## Примеры
1. Свойство как свойство другой модели
    ```php
    * @SWG\Property(
    *     property="category_id",
    *     ref="#/definitions/Category/properties/id"
    * )
    * @var string 
    ```

2. Свойство как другая модель
    ```php
    * @SWG\Property(
    *     property="image",
    *     ref="#/definitions/Image"
    * )
    * @var string 
    ```
3. Свойство как массив
    ```php
    /**
     * @SWG\Property(
     *     type="array",
     *     property="items",
     *     @SWG\Items(
     *         @SWG\Property(
     *             property="id",
     *             ref="#/definitions/CommonProperties/properties/id"
     *         ),
     *         @SWG\Property(
     *             property="title",
     *             ref="#/definitions/CommonProperties/properties/title"
     *         ),
     *     )
     * ),
     */
    ```
[К Оглавлению](./../../README.md)