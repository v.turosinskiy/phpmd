# @SWG\Post

## Описание
Класс [Swagger\Annotations\Post](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Post.php)

Описывает структуру Post запросов. Наследник класса [@SWG\Operation](./Operation.md)

[К Оглавлению](./../../README.md)