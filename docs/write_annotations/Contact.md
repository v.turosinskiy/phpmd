# @SWG\Contact

## Описание
Класс [Swagger\Annotations\Contact](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Contact.php)

OpenAPI Specification 2: [Contact Object](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#contactObject)

## Родительские аннотации
- [@SWG\Info](Info.md)

## Свойства аннотации
- `name` (string)
- `url` (string)
- `email` (string)

## Пример
```php
/**
 * @SWG\Info(
 *     version="1.0.0",
 *     title="Swagger Petstore",
 *     @SWG\Contact(name="Swagger API Team")
 * ),
 */
```

[К Оглавлению](./../../README.md)