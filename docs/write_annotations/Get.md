# @SWG\Get

## Описание
Класса [Swagger\Annotations\Get](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Get.php)

Описывает структуру GET запросов. Наследник класса [@SWG\Operation](./Operation.md)
## Родительские аннотации
1. [@SWG\Swagger](Swagger.md)

## Свойства аннотации
- `path` (string) 
- `method` (string) 
- `tags` (string)
- `summary` (string)
- `description` (string)
- `externalDocs` (string)
- `operationId` (string)
- `consumes` (array)
- `produces` (array)
- `parameters` [@SWG\Parameter[]](Parameter.md)
- `responses` (array)
- `schemes` (array)
- `deprecated` (boolean)
- `security` (array)

## Примеры
```php
/*
 * @SWG\Get(
 *     path="/api/newsfeed/v4/backend/json/{lang}/articles",
 *     summary="Получить список статей",
 *     tags={"Статьи "},
 *     description="# Метод для получения списка статей",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *         ref="#/parameters/lang"
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="successful operation",
 *     ),
 * )
 */
```

[К Оглавлению](./../../README.md)