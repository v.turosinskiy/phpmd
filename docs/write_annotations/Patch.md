# @SWG\Patch

## Описание
Класс [Swagger\Annotations\Patch](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Patch.php)

Описывает структуру Patch запросов. Наследник класса [@SWG\Operation](./Operation.md)
## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации

## Примеры

[К Оглавлению](./../../README.md)