# @SWG\Info

## Описание
Класс [Swagger\Annotations\Info](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Info.php)

## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации

## Примеры

```php
 * @SWG\Info(
 *     version="1.0.0",
 *     title="Swagger Petstore",
 *     description="A sample API that uses a petstore as an example to demonstrate features in the swagger-2.0 specification",
 *     termsOfService="http://swagger.io/terms/",
 *     @SWG\Contact(name="Swagger API Team"),
 *     @SWG\License(name="MIT")
 * )
```

[К Оглавлению](./../../README.md)