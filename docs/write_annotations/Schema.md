# @SWG\Schema

## Описание
Класс [Swagger\Annotations\Schema](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Schema.php)

## Родительские аннотации
- [@SWG\Response](./Response.md)
- [@SWG\Parameter](./Parameter.md)
## Дочерние аннотации
- [@SWG\Property](./Property.md)
- [@SWG\Items](./Items.md)
- [@SWG\ExternalDocumentation](./ExternalDocumentation.md)
- [@SWG\Xml](./XML.md)
## Свойства аннотации
- `description`
    - Тип `string`,
- `required`
    - Тип `string`,
- `format`
    - Тип `string`,
- `collectionFormat`
    - Возможные значения ['csv', 'ssv', 'tsv', 'pipes', 'multi'],
- `maximum` 
    - Тип `number`
- `exclusiveMaximum`
    - Тип `boolean`,
- `minimum`
    - Тип `number`,
- `exclusiveMinimum`
    - Тип `boolean`,
- `maxLength`
    - Тип `integer`,
- `minLength`
    - Тип `integer`,
- `pattern`
    - Тип `string`,
- `maxItems`
    - Тип `integer`,
- `minItems`
    - Тип `integer`,
- `uniqueItems`
    - Тип `boolean`,
- `multipleOf`
    - Тип `integer`,

## Примеры
1. Схема модели
    ```php
    /**
     * @SWG\Schema(
     *     ref="#/definitions/Article"
     * )
     */
    ```

2. Список моделей с постраничной навигацией
    ```php
    /**
     * @SWG\Schema(
     *     @SWG\Property(
     *         type="array",
     *         property="items",
     *         @SWG\Items(
     *             ref="#/definitions/Article"
     *         )
     *      ),
     *      @SWG\Property(
     *         property="links",
     *         ref="#/definitions/CommonProperties/properties/_links"
     *      ),
     *      @SWG\Property(
     *         property="_meta",
     *         ref="#/definitions/CommonProperties/properties/_meta"
     *      ),
     * )
     */
    ```
3. Список свойств с постраничной навигацией
    ```php
    /**
    * @SWG\Schema(
    *     @SWG\Property(
    *         type="array",
    *         property="items",
    *         @SWG\Items(
    *             @SWG\Property(
    *                 property="id",
    *                 ref="#/definitions/CommonProperties/properties/id"
    *             ),
    *             @SWG\Property(
    *                 property="title",
    *                 ref="#/definitions/CommonProperties/properties/title"
    *             ),
    *         )
    *      ),
    *      @SWG\Property(
    *         property="links",
    *         ref="#/definitions/CommonProperties/properties/_links"
    *      ),
    *      @SWG\Property(
    *         property="_meta",
    *         ref="#/definitions/CommonProperties/properties/_meta"
    *      ),
    * )
    */
    ```
4. Расширенная модель (модель + дополнительные свойства)
    ```php
    /**
    * @SWG\Schema(
    *     allOf={
    *         @SWG\Property(
    *             ref="#/definitions/BackendPressMaterialItem",
    *         ),
    *         @SWG\Schema(
    *             type="object",
    *             @SWG\Property(
    *                 property="preview",
    *                 ref="#/definitions/CommonProperties/properties/preview",
    *             ),
    *         )
    *     )
    * )
    */
    ```
[К Оглавлению](./../../README.md)