# @SWG\ExternalDocimentation

## Описание
Класс [Swagger\Annotations\ExternalDocimentation](https://github.com/zircote/swagger-php/blob/master/src/Annotations/ExternalDocumentation.php)

## Родительские аннотации
- [@SWG\Swagger](Swagger.md)
- [@SWG\Tag](Tag.md)
- [@SWG\Schema](Schema.md)
- [@SWG\Definition](Definition.md)
- [@SWG\Property](Property.md)
- [@SWG\Operation](Operation.md)
- [@SWG\Get](Get.md)
- [@SWG\Post](Post.md)
- [@SWG\Put](Put.md)
- [@SWG\Delete](Delete.md)
- [@SWG\Patch](Patch.md)
- [@SWG\Head](Head.md)
- [@SWG\Options](Options.md)
- [@SWG\Items](Items.md)

## Свойства аннотации
- `description` (string)
- `url` (string)

## Примеры
```php
  "externalDocs": {
    "description": "find more info here",
    "url": "https://swagger.io/about"
  },
```
[К Оглавлению](./../../README.md)