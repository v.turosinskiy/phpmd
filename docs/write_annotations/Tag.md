# @SWG\Tag

## Описание
Класс [Swagger\Annotations\Tag](https://github.com/zircote/swagger-php/blob/master/src/Annotations/Tag.php)

## Родительские аннотации

## Дочерние аннотации

## Свойства аннотации
- `name` - имя тэга(модели)
- `description` - краткое описание назначения тэга
- `externalDocs` - ссылка на подробную документацию

## Примеры

```php
/*
 * @SWG\Tag(
 *   name="Статьи ",
 *   description="Административные методы"
 * )
 */
```

[К Оглавлению](./../../README.md)