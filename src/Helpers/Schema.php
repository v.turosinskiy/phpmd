<?php

namespace SwaggerMD\Helpers;

use Swagger\Annotations\Swagger;

class Schema
{
    private $swagger;

    public function __construct(Swagger $swagger)
    {
        $this->swagger = $swagger;
    }

    public function getModelBySchema($schema)
    {
        $model = null;

        if (!empty($schema['properties']['items']['items']['$ref'])) {
            $model = $this->getSwagger()->ref($schema['properties']['items']['items']['$ref']);
        } elseif (!empty($schema['properties']['fields']['items']['properties'])) {
            $model = $schema['properties']['fields']['items'];
        } elseif (!empty($schema['properties']['items']['items']['properties'])) {
            $model = $schema['properties']['items']['items'];
        } elseif (!empty($schema['$ref'])) {
            $model = $this->getSwagger()->ref($schema['$ref']);
        } elseif (!empty($schema['allOf']) || !empty($schema['properties']['items']['items']['allOf'])) {
            $elementSchema = (!empty($schema['properties']['items']['items']['allOf'])) ? $schema['properties']['items']['items']['allOf'] : $schema['allOf'];
            $properties = [];
            foreach ($elementSchema as $childSchema) {
                if (!empty($childSchema['$ref'])) {
                    $refModel = $this->getSwagger()->ref($childSchema['$ref']);
                    $properties = array_merge($properties, $refModel['properties']);
                } elseif (!empty($childSchema['properties'])) {
                    $properties = array_merge($properties, $childSchema['properties']);
                }
            }
            if (!empty($properties)) {
                $model = [
                    'properties' => $properties
                ];
            }
        } elseif (!empty($schema['properties'])) {
            $model = $schema;
        }

        return $model;
    }

    public function getSwagger()
    {
        return $this->swagger;
    }

    public function getExampleBySchema($schema)
    {
        $examples = null;
        if (!empty($schema['example'])) {
            $examples = $schema['example'];
        } elseif (!empty($schema['default'])) {
            $examples = $schema['default'];
        } elseif (!empty($schema['allOf'])) {
            $examples = [];
            foreach ($schema['allOf'] as $childSchema) {
                $examples = array_merge($examples, $this->getExampleBySchema($childSchema));
            }
        } elseif (!empty($schema['$ref'])) {
            $model = $this->getSwagger()->ref($schema['$ref']);
            $examples = $this->getExampleByModel($model);
        } elseif (!empty($schema['properties'])) {
            $examples = $this->getExampleByModel($schema);
        } elseif (!empty($schema['type'])) {
            if (!empty($schema['items'])) {
                $examples = $this->getExampleByItems($schema['items']);
            } else {
                $examples = $schema['type'];
            }
        }
        return $examples;
    }

    public function getExampleByModel($model)
    {
        $example = null;
        if (!empty($model['example'])) {
            $example = $model['example'];
        } elseif (!empty($model['default'])) {
            $example = $model['default'];
        } elseif (!empty($model['properties'])) {
            $example = [];
            foreach ($model['properties'] as $key => $property) {
                $example[$key] = $this->getExampleByProperty($property);
            }
        } elseif (!empty($property['type'])) {
            $example = $this->getExampleByProperty($property);
        }
        return $example;
    }

    public function getExampleByProperty($property)
    {
        $example = null;
        if (!empty($property['example'])) {
            $example = $property['example'];
        } elseif (!empty($property['default'])) {
            $example = $property['default'];
        } elseif (!empty($property['properties'])) {
            $example = $this->getExampleByModel($property);
        } elseif (!empty($property['items'])) {
            $example = $this->getExampleByItems($property['items']);
        } elseif (!empty($property['type'])) {
            if (!empty($property['enum'])) {
                $example = $property['enum'][0];
            } else {
                $example = $property['type'];
            }
        } elseif (!empty($property['$ref'])) {
            preg_match("/(.*)\/properties\/(.*)/", $property['$ref'], $output_array);
            if (!empty($output_array)) {
                $ref = $output_array[1];
                $propertyName = $output_array[2];
                $model = $this->getSwagger()->ref($ref);
                $example = $this->getExampleByProperty($model['properties'][$propertyName]);
            } else {
                $model = $this->getSwagger()->ref($property['$ref']);
                $example = $this->getExampleByModel($model);
            }
        }
        return $example;
    }

    public function getExampleByItems($items, $count = 1)
    {
        $examples = null;
        if (!empty($items['$ref'])) {
            $model = $this->getSwagger()->ref($items['$ref']);
            $example = $this->getExampleByModel($model);
        } elseif (!empty($items['allOf'])) {
            $example = [];
            foreach ($items['allOf'] as $childSchema) {
                $example = array_merge($example, $this->getExampleBySchema($childSchema));
            }
        } elseif (!empty($items['properties'])) {
            $example = $this->getExampleByModel($items);
        } elseif (!empty($items['type'])) {
            if ($items['type'] == 'array') {
                $example = $this->getExampleByItems($items['items']);
            } else {
                $example = $this->getExampleByProperty($items);
            }
        }

        if (!empty($example)) {
            for ($i = 0; $i < $count; $i++) {
                $examples[] = $example;
            }
        }
        return $examples;
    }
}