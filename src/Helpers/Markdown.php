<?php

namespace SwaggerMD\Helpers;

use Swagger\Annotations\Swagger;

/**
 * @property \Swagger\Annotations\Swagger $swagger
 */
class Markdown
{
    protected $swagger;
    protected $actionPath;

    public function setActionPath($actionPath){
        $this->actionPath = $actionPath;
    }

    public function __construct(Swagger $swagger = null)
    {
        $this->swagger = $swagger;
    }

    public function getActionDescription($action)
    {
        $md = '';
        $additionalColumns = [];
        $responses = $action['responses'];
        if (empty($responses)) {
            return '';
        }

        if (!empty($action['parameters'])) {
            foreach ($action['parameters'] as $parameter) {
                if (!empty($parameter['$ref'])) {
                    $refParameter = $this->getSwagger()->ref($parameter['$ref']);
                    $parameter = array_merge($refParameter, $parameter);
                }

                if (!empty($parameter['name'])) {
                    switch ($parameter['name']) {
                        case 'filter':
                        case 'sort':
                            $name = $parameter['name'];
                            $additionalColumns[$name] = (!empty($action['x-' . $name])) ? $action['x-' . $name] : [];
                            break;
                        case 'expand':
                            if (!empty($parameter['items']['enum'])) {
                                $additionalColumns['expand'] = $parameter['items']['enum'];
                            }
                            if (!empty($parameter['items']['x-ref'])) {
                                $additionalColumns['expand-ref'] = $parameter['items']['x-expand-ref'];
                            }
                            break;
                    }
                }
            }
        }

        foreach ($responses as $code => $response) {
            if (!empty($code) && $code == 200 && !empty($response['description'])) {
                $md .= 'Успешный ответ приходит с кодом `' . strtoupper($code) . ' ' . $response['description'] . '`';
                if (!empty($response['schema'])) {
                    $additionalColumns['optional'] = [];
                    if (!empty($response['schema']['x-optional'])) {
                        $additionalColumns['optional'] = $response['schema']['x-optional'];
                    }
                    $md .= ' и содержит тело:' . PHP_EOL;
                    $md .= PHP_EOL . $this->getJson($this->getExampleByResponse($response)) . PHP_EOL;
                    if (!empty($response['schema'])) {
                        $options = [
                            'additionalColumns' => $additionalColumns
                        ];
                        $tables = $this->getResponseDescription($response, $options);
                        if (!empty($tables['main'])) {
                            $md .= '<a name="fields"></a>' . PHP_EOL;
                            $md .= '## Список возвращаемых полей' . PHP_EOL;
                            $md .= $tables['main'] . PHP_EOL;
                        }
                        if (!empty($tables['additional'])) {
                            $md .= $tables['additional'];
                        }
                    }
                } else {
                    $md .= PHP_EOL;
                }
            }
        }

        return $md;
    }

    public function getSwagger()
    {
        return $this->swagger;
    }

    public function getJson($vars)
    {
        if (empty($vars)) {
            return '';
        }

        return '```json' . PHP_EOL . json_encode($vars, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) . PHP_EOL . '```';
    }

    public function getExampleByResponse($vars)
    {
        $examples = null;
        if (!empty($vars['schema'])) {
            $schema = new Schema($this->getSwagger());
            $examples = $schema->getExampleBySchema($vars['schema']);
        }

        return $examples;
    }

    public function getResponseDescription($response, $options = [])
    {
        $tables = null;
        $schema = new Schema($this->getSwagger());
        $model = $schema->getModelBySchema($response['schema']);

        if (!empty($model)) {
            $tables = $this->getPropertiesByModel($model, $options);
        }
        return $tables;
    }

    public function getPropertiesByModel($model, $options = [])
    {
        $output = [];
        if (empty($model['properties'])) {
            return $output;
        }

        foreach ($model['properties'] as $name => &$property) {
            $property['name'] = $name;
        }
        unset($property);

        $tables = $this->getTables($model['properties'], $options);

        return $tables;
    }

    public function getTables($items, $options = [])
    {
        if (empty($items)) {
            return null;
        }

        $tables = [];
        $schema = new Schema($this->getSwagger());
        $extra = [];
        $rows = [];
        $columnKeys = [];
        $columns = [
            'key'        => 'Ключ',
            'in'         => 'Передается в',
            'short_name' => 'Название',
            'type'       => 'Тип',
            'required'   => 'Обязательный',
            'filterable' => 'Фильтрация',
            'sortable'   => 'Сортировка',
            'optional'   => 'По умолчанию',
            'on_update'  => 'При сохранении/обновлении'
        ];

        $additionalColumns = (!empty($options['additionalColumns'])) ? $options['additionalColumns'] : [];

        $expandFields = [];
        if (!empty($additionalColumns['expand'])) {
            foreach ($additionalColumns['expand'] as $name) {
                if (empty($items[$name])) {
                    $expandFields[] = $name;
                    $refKey = (!empty($additionalColumns['expand-ref'][$name])) ? $additionalColumns['expand-ref'][$name] : $name;
                    $ref = '#/definitions/CommonProperties/properties/' . $refKey;
                    $item = ['$ref' => $ref, 'name' => $name];
                    $refItem = $this->getPropertyByRef($item);
                    if (!empty($refItem)) {
                        $items[$name] = $refItem;
                    }
                }
            }
        }

        foreach ($items as $itemKey => $item) {
            if (!empty($item['$ref'])) {
                $refItem = $this->getPropertyByRef($item);
                $item = array_merge($refItem, $item);
            }

            if (empty($item['optional'])) {
                if (in_array($item['name'], $expandFields)) {
                    $item['optional'] = 'expand';
                } elseif (!empty($additionalColumns['optional'][$item['name']])) {
                    $item['optional'] = '*';
                }
            }

            if (array_key_exists('filter', $additionalColumns)) {
                $filterable = true;
                if (!empty($additionalColumns['filter']) && !in_array($item['name'], $additionalColumns['filter'])) {
                    $filterable = false;
                } elseif (!empty($item['x-filterable']) && $item['x-filterable'] === false) {
                    $filterable = false;
                }
                $item['filterable'] = $filterable;
            }

            if (array_key_exists('sort', $additionalColumns)) {
                $sortable = true;
                if (!empty($additionalColumns['sort']) && !in_array($item['name'], $additionalColumns['sort'])) {
                    $sortable = false;
                } elseif (!empty($item['x-sortable']) && $item['x-sortable'] === false) {
                    $sortable = false;
                }
                $item['sortable'] = $sortable;
            }

            $item['key'] = $item['name'];

            if (in_array('formExample', $options) && !empty($item['type']) && in_array($item['type'], ['object', 'array'])) {
                if (empty($item['x-formExample'])) {
                    $item['x-formExample'] = $schema->getExampleByProperty($item);
                }
            }

            if (!empty($item['model_path'])) {
                $item['type'] = ($item['type'] == 'array') ? 'array object' : 'object';
            } elseif (!empty($item['type']) && !empty($item['enum'])) {
                $item['type'] = 'enum ' . $item['type'];
                if (empty(array_diff($item['enum'], [0, 1]))) {
                    unset($item['enum']);
                }
            } elseif (!empty($item['type']) && $item['type'] == 'array') {
                if (!empty($item['items']['ref'])) {
                    $refItem = $this->getPropertyByRef($item['items']['ref']);
                    $item['items'] = array_merge($refItem, $item['items']);
                }
                if (!empty($item['items']['enum'])) {
                    $item['type'] = 'array enum ' . $item['items']['type'];
                }
            }

            if (!empty($item['x-nullable']) && $item['x-nullable'] === true) {
                $item['type'] .= ' / null';
            }

            if (!empty($item['description'])) {
                $spliDesc = self::splitDescription($item['description']);

                if (!empty($spliDesc['short_name'])) {
                    $item['short_name'] = $spliDesc['short_name'];
                }

                if (!empty($spliDesc['full_description'])) {
                    $item['full_description'] = $spliDesc['full_description'];
                }
            } else {
                $item['short_name'] = 'Без описания';
            }

            if (!empty($item['x-deprecated'])) {
                $item['short_name'] .= ' (Deprecated)';
            }

            foreach ($item as $field => $value) {
                if (!empty($columns[$field]) && !in_array($field, $columnKeys)) {
                    $columnKeys[] = $field;
                }
            }

            $items[$itemKey] = $item;
        }

        foreach ($columns as $field => $column) {
            if (!in_array($field, $columnKeys)) {
                unset($columns[$field]);
            }
        }

        $links = [];

        foreach ($items as $itemKey => $item) {
            if (!empty($item['key'])) {
                // Проверяем нужно ли выводить дополнительное
                // описание для свойства после таблицы со свойсвами
                $extraKeys = ['default', 'full_description', 'enum', 'optional', 'minLength', 'maxLength'];
                if (in_array('formExample', $options)) {
                    $extraKeys[] = 'x-formExample';
                }
                $itemKeys = array_keys($item);
                $hasExtraFields = array_intersect($itemKeys, $extraKeys);

                if (!empty($hasExtraFields)) {
                    $extra[$item['key']] = $item;
                }

                if (!empty($item['x-post'])) {
                    $postProperties[] = $item;
                }

                $row = [];
                foreach ($columns as $key => $column) {
                    switch ($key) {
                        case 'filterable':
                            $value = ($item['filterable'] == true) ? '+' : '-';
                            break;
                        case 'sortable':
                            $value = ($item['type'] != 'object' && $item['sortable'] == true) ? '+' : '-';
                            break;
                        case 'required':
                            $value = (!empty($item[$key]) && $item[$key] === true) ? '+' : '-';
                            break;
                        case 'optional':
                            $value = (empty($item[$key])) ? '+' : $item[$key];
                            break;
                        case "short_name":
                            $name = $item['short_name'];
                            if (!empty($item['model_path'])) {
                                $links[] = [
                                    'name' => $name,
                                    'path' => $item['model_path']
                                ];
                                $value = '[' . $name . ']';
                            } else {
                                $value = (array_key_exists($item['key'],
                                    $extra)) ? '[' . $name . '](' . '#' . $item['key'] . ')' : $name;
                            }

                            break;
                        default:
                            $value = (!empty($item[$key])) ? $item[$key] : 'Не указано';
                    }

                    $row[$key] = $value;
                }
                $rows[] = $row;
            }
        }

        if (empty($rows)) {
            return [];
        }

        $align = [];
        foreach ($columns as $key => $column) {
            $align[$key] = 'L';
        }

        $t = new TextTable($columns, $rows, $align);
        $t->maxlen = 500;

        $content = $t->render() . PHP_EOL;

        if (!empty($links)) {
            $content .= PHP_EOL;
            foreach ($links as $link) {
                $content .= '[' . $link['name'] . ']: ' . $link['path'] . PHP_EOL;
            }
        }

        $tables['main'] = $content;
        if (empty($extra)) {
            return $tables;
        }

        $additional = '';
        if (!empty($extra)) {
            foreach ($extra as $item) {
                $extraDescription = [];
                $additional .= '<a name="' . $item['key'] . '"></a>' . PHP_EOL;
                $extraDescription[] .= '`' . $item['key'] . '` - **' . $item['short_name'] . '** ';

                if (!empty($item['full_description'])) {
                    $extraDescription[] = $item['full_description'];
                }

                if (isset($item['default'])) {
                    $extraDescription[] = 'Значение по умолчанию: `' . $item['default'] . '`';
                }

                if (!empty($additionalColumns['optional'][$item['name']])) {
                    $extraDescription[] = '`*` - ' . $additionalColumns['optional'][$item['name']];
                    if (!empty($item['optional']) && $item['optional'] == 'expand') {
                        $schema = new Schema($this->getSwagger());
                        $example = $schema->getExampleByProperty($item);
                        $extraDescription[] = $this->getJson([$item['name'] => $example]);
                    }
                }

                if (!empty($item['enum'])) {
                    $str = 'Cписок возможных значений: ';
                    $str .= '`' . implode('`, `', $item['enum']) . '`';
                    $extraDescription[] = $str;
                }

                if (!empty($item['maxLength'])) {
                    $str = 'Максимальное кол-во символов: ' . $item['maxLength'];
                    $extraDescription[] = $str;
                }

                if (!empty($item['minLength'])) {
                    $str = 'Минимальное кол-во символов: ' . $item['minLength'];
                    $extraDescription[] = $str;
                }

                if (!empty($item['x-formExample'])) {
                    $str = 'Пример: ' . PHP_EOL;
                    $str .= $this->getJson($item['x-formExample']);
                    $extraDescription[] = $str;
                }

                $additional .= implode('    ' . PHP_EOL, $extraDescription) . PHP_EOL . PHP_EOL;
            }
            $additional .= '    ' . PHP_EOL;
        }

        $tables['additional'] = $additional;
        return $tables;
    }

    public function getPropertyByRef($property)
    {
        $swagger = $this->getSwagger();
        if (!empty($property['$ref']) && stristr($property['$ref'], '#/')) {
            $refProperty = $swagger->ref($property['$ref']);
            preg_match("/#\/definitions\/CommonTypes\/properties\/(.+)/", $property['$ref'], $output_array);
            if (!empty($output_array[1])) {
                $property['type'] = $output_array[1];
                switch ($output_array[1]) {
                    case 'datetimeMaterial':
                    case 'datetimeDefault':
                        $property['type'] = 'datetime';
                        break;
                    case 'dateDefault':
                        $property['type'] = 'date';
                        break;
                    default:
                        $property['type'] = $output_array[1];
                }
            } else {
                preg_match("/#\/definitions\/CommonProperties\/properties\/(.+)/", $property['$ref'], $output_array);
                if (!empty($output_array[1])) {
                    if (!empty($refProperty['$ref'])) {
                        $ref = $refProperty['$ref'];
                    } elseif (!empty($refProperty['type']) && $refProperty['type'] == 'array' && !empty($refProperty['items']['$ref'])) {
                        $ref = $refProperty['items']['$ref'];
                    }

                    if (!empty($ref)) {
                        preg_match("/#\/definitions\/([^\/]+)$/", $ref, $ref_output_array);
                        if (!empty($ref_output_array[1]) && $ref_output_array[1] != 'CommonTypes') {
                            $path = $this->getActionPath();
                            $property['model_path'] = $path . '/models/' . $ref_output_array[1] . '/README.md';
                        }
                    }
                }
            }
            unset($property['$ref']);
            if (!empty($refProperty['$ref'])) {
                $refProperty = $this->getPropertyByRef($refProperty);
            }
            $property = array_merge($refProperty, $property);
        }
        return $property;
    }

    public function getActionPath()
    {
        return $this->actionPath;
    }

    public static function splitDescription($description)
    {
        $result = [];
        $pos = strpos($description, "\n");
        if (!empty($pos)) {
            $result['short_name'] = trim(substr($description, 0, $pos));
            $result['full_description'] = trim(substr($description, $pos, strlen($description)));
        } else {
            $result['short_name'] = $description;
        }
        return $result;
    }
}