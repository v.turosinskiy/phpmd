<?php

namespace SwaggerMD;

use Swagger\Annotations\Swagger;
use SwaggerMD\Helpers\Markdown;
use SwaggerMD\Helpers\Schema;
use SwaggerMD\Helpers\TextTable;

/**
 * Генератор документации
 *
 * @property $config
 * @property $logger
 */
class Generator
{
    protected $swagger;
    protected $json;
<<<<<<< HEAD
    protected $output;
=======
    protected $directory;
>>>>>>> cc47a571015a6398203034ada379f3ddb7fbf441
    protected $twig;
    protected $properties;
    protected $schema;
    protected $markdown;

    public function __construct($json)
    {
        $loader = new \Twig_Loader_Filesystem(dirname(__DIR__) . '/templates');
        $this->twig = new \Twig_Environment($loader);
        $this->properties = json_decode($json, true);
        $this->swagger = new Swagger($this->properties);
        $this->schema = new Schema($this->swagger);
        $this->markdown = new Markdown($this->swagger);
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function getSchema()
    {
        return $this->schema;
    }

    public function getMarkdown()
    {
        return $this->markdown;
    }

    public function getSwagger()
    {
        return $this->swagger;
    }

<<<<<<< HEAD
    public function saveTo($output)
    {
        $this->output = $output;
=======
    public function saveTo($directory)
    {
        $this->clearDirectory($directory);
        $this->directory = $directory;
>>>>>>> cc47a571015a6398203034ada379f3ddb7fbf441
        $data = $this->getIndex();
        $this->generateSingleFile($data);

        $data = $this->getSearch($data);
        $this->generateSingleFile($data);

        $parameters = $this->getParameters();
        $this->generateSingleFile($parameters);

        $actions = $this->getActions();
        if (!empty($actions)) {
            foreach ($actions as $data) {
                $this->generateSingleFile($data);
            }
        }

        $data = $this->getListModels();
        if (!empty($data)) {
            $this->generateSingleFile($data);
        }

        $models = $this->getModels();
        if (!empty($models)) {
            foreach ($models as $model) {
                $this->generateSingleFile($model);
            }
        }
    }

    public function getScanDirs()
    {
        return $this->config['scan'];
    }

    public function getPathToGenerator()
    {
        return dirname(__DIR__);
    }

    public function clearDirectory($directory)
    {
        if (!empty($directory) && is_dir($directory)) {
            $this->delTree($directory);
        }
    }

    public function getPathToDocs()
    {
<<<<<<< HEAD
        return $this->output;
=======
        return $this->directory;
>>>>>>> cc47a571015a6398203034ada379f3ddb7fbf441
    }

    public function delTree($dir)
    {
        $files = array_diff(scandir($dir), ['.', '..']);
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
<<<<<<< HEAD
        return ;
=======
        return rmdir($dir);
>>>>>>> cc47a571015a6398203034ada379f3ddb7fbf441
    }

    public function saveFile($json)
    {
        $file = $this->getPathToJson();
        $this->createDirByPath(dirname($file));
        file_put_contents($file, $json);
    }

    public function getPathToJson()
    {
        return $this->config['json'];
    }

    public function createDirByPath($path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }
    }

    public function getIndex()
    {
        $context = [];
        $properties = $this->getProperties();
        $docDirectory = $this->getPathToDocs();
        $tags = [];

        if (!empty($properties['tags'])) {
            foreach ($properties['tags'] as $tag) {
                if (!empty($tag['name']) && !empty($tag['description'])) {
                    $tags[$tag['name']] = $tag['description'];
                }
            }
        }

        foreach ($properties['paths'] as $actionPath => $actionsList) {
            foreach ($actionsList as $method => $action) {
                if (!empty($action['tags']) && !empty($action['summary'])) {
                    $path = self::getDirByPath('actions' . $actionPath);
                    $method = strtoupper($method);
                    $file = $path . '/' . $method . '.md';
                    if (!empty($action['deprecated']) && $action['deprecated'] === true) {
                        $action['summary'] .= ' (Deprecated)';
                    }
                    foreach ($action['tags'] as $tag) {
                        $name = trim($tag);
                        $is_frontend = (empty($action['security'])) ? true : false;
                        $context['tags'][$name][] = [
                            'file'        => $file,
                            'title'       => $action['summary'],
                            'path'        => $actionPath,
                            'method'      => $method,
                            'is_frontend' => $is_frontend
                        ];
                    }
                }
            }
        }

        ksort($context['tags']);
        foreach ($context['tags'] as $name => $actions) {
            usort($actions, function ($a, $b) {
                if ($a['title'] == $b['title']) {
                    return 0;
                }
                return ($a['title'] < $b['title']) ? -1 : 1;
            });

            $context['tags'][$name] = $actions;
        }

        $context['images'] = [
            'frontend' => '<img src="https://image.ibb.co/mGJbMc/frontend.png" alt="frontend" />',
            'backend'  => '<img src="https://image.ibb.co/h9bVgc/backend.png" alt="backend" />',
        ];

        $file = $docDirectory . '/README.md';
        $vars = [
            'template' => 'index.twig',
            'file'     => $file,
            'context'  => $context
        ];
        return $vars;
    }

    public static function getDirByPath($path)
    {
        $path = str_replace(['{', '}', '\\'], ['', '', DIRECTORY_SEPARATOR], $path);
        return $path;
    }

    public function getPathToTemplate($templateType)
    {
        $file = $this->getPathToGenerator() . '/templates/md/' . $templateType . '.tpl.php';
        return (file_exists($file)) ? $file : null;
    }

    public function generateSingleFile($data)
    {
        if (!empty($data['file']) && !empty($data['template']) && !empty($data['context'])) {
            $file = $data['file'];
            $template = $data['template'];
            $context = $data['context'];
            $this->createDirByPath(dirname($file));
            $str = $this->twig->load($template)->render($context);
            file_put_contents($file, $str);
        }
    }

    public function template($file, $args)
    {
        if (!is_file($file) && is_readable($file)) {
            return null;
        }

        if (is_array($args)) {
            extract($args);
        }

        ob_start();
        include $file;
        return ob_get_clean();
    }

    public function getSearch($data)
    {
        $table = [];
        $rows = [];
        $columns = [
            'tag'    => 'Тэг',
            'title'  => 'Название метода',
            'method' => 'Тип метода',
            'path'   => 'Путь',
        ];

        $links = [];

        foreach ($data['context']['tags'] as $tag => $actions) {
            foreach ($actions as $template) {
                if (!empty($template['title']) && !empty($template['file']) && !empty($template['method']) && !empty($template['path'])) {
                    if (strlen($template['title']) > 70) {
                        $template['title'] = substr($template['title'], 0, 67);
                        $template['title'] .= '...';
                    }
                    if (empty($template['file'])) {
                        $title = $template['title'];
                    } else {
                        $title = $template['title'];
                        while (!empty($links[$title])) {
                            $title .= ' ';
                        }
                    }

                    $file = '../' . $template['file'];
                    $links[$title] = '[' . $title . ']: ' . $file;

                    $title = '[' . $title . ']';
                    $method = $template['method'];
                    $path = $template['path'];
                    $rows[] = [
                        'tag'    => $tag,
                        'title'  => $title,
                        'method' => $method,
                        'path'   => $path
                    ];

                }
            }
        }

        if (!empty($columns)) {
            $t = new TextTable($columns, $rows);
            $table = $t->render();
        }

        $data['context']['links'] = implode(PHP_EOL, $links);
        $data['context']['table'] = $table;
        $data['template'] = 'search.twig';
        $path = $this->getPathToDocs();
        $data['file'] = $path . '/search/README.md';
        return $data;
    }

    public function getParameters()
    {
        $properties = $this->getProperties();
        if (empty($properties['parameters'])) {
            return false;
        }
        $data = null;
        $path = $this->getPathToDocs();
        if (!empty($path)) {
            $file = $path . '/parameters/README.md';
            $context = [
                'parameters' => $properties['parameters']
            ];
            $md = $this->getMarkdown();
            if (!empty($properties['parameters'])) {
                $tables = $md->getTables($properties['parameters']);

                if (!empty($tables['main'])) {
                    $context['mainTable'] = $tables['main'];
                }

                if (!empty($tables['additional'])) {
                    $context['additionalTable'] = $tables['additional'];
                }
            }

            $data = [
                'file'     => $file,
                'template' => 'parameters.twig',
                'context'  => $context
            ];
        }
        return $data;
    }

    public function getActions()
    {
        $properties = $this->getProperties();
        $data = [];
        $docDirectory = $this->getPathToDocs();

        foreach ($properties['paths'] as $actionPath => $actionsList) {
            foreach ($actionsList as $method => $action) {
                $path = self::getDirByPath($docDirectory . '/actions' . $actionPath);
                $template = 'action.twig';
                $method = strtoupper($method);
                if (!empty($template) && !empty($path)) {
                    $file = $path . '/' . $method . '.md';
                    $docPath = '..' . preg_replace("/([^\/]+)/", "..", $actionPath);
                    $md = $this->getMarkdown();
                    $md->setActionPath($docPath);

                    $context = [
                        'method'  => $method,
                        'path'    => $actionPath,
                        'docPath' => $docPath,
                    ];

                    if (empty($action['parameters'])) {
                        $action['parameters'] = [];
                    }

                    $keys = [];

                    foreach ($action['parameters'] as $key => &$parameter) {
                        $parameter = array_merge($md->getPropertyByRef($parameter), $parameter);
                        if (!in_array($parameter['name'], $keys)) {
                            $keys[] = $parameter['name'];
                        }
                    }
                    unset($parameter);


                    if (!empty($action['x-action'])) {
                        $actionData = explode('/', $action['x-action']);
                        $definition = $actionData[1];
                        $actionType = $actionData[0];
                        if (!empty($definition) && !empty($actionType)) {
                            $ref = '#/definitions/' . $definition;
                            $model = ['$ref' => $ref];
                            $refItem = $md->getPropertyByRef($model);
                            foreach ($refItem['properties'] as $key => $property) {
                                if (!in_array($key, $keys)) {
                                    $refProperty = $md->getPropertyByRef($property);
                                    if (!empty($refProperty['description'])) {
                                        if (!empty($refProperty['readOnly']) && $refProperty['readOnly'] === true) {
                                            continue;
                                        }
                                        $parameter = [
                                            'name'        => $key,
                                            'in'          => 'formData',
                                            'description' => $refProperty['description'],
                                            'type'        => $refProperty['type']
                                        ];

                                        if (!empty($refItem['required']) && in_array($key, $refItem['required'])) {
                                            $parameter['required'] = true;
                                        }
                                        if (!empty($refProperty['items'])) {
                                            $parameter['items'] = $refProperty['items'];
                                        }
                                        if (!empty($refProperty['x-formData'])) {
                                            $parameter['description'] .= PHP_EOL . '    ' . $this->formatString($refProperty['x-formData']);
                                        }
                                        if (!empty($refProperty['x-formExample'])) {
                                            $example = $this->formatExample($refProperty['x-formExample']);
                                            if (!empty($example)) {
                                                $parameter['description'] .= $example;
                                            }
                                        }
                                        if (!empty($parameter)) {
                                            $action['parameters'][] = $parameter;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!empty($action['description'])) {
                        $action['description'] = $this->formatString($action['description']);
                    }

                    $parameters = [];
                    $reqParameters = [];

                    if (!empty($action['parameters'])) {
                        foreach ($action['parameters'] as $parameter) {
                            if (!empty($parameter['required']) && $parameter['required'] === true) {
                                $reqParameters[] = $parameter;
                            } else {
                                $parameters[] = $parameter;
                            }
                        }
                    }

                    if (!empty($reqParameters)) {
                        $tables = $md->getTables($reqParameters, ['formExample']);
                        if (!empty($tables['main'])) {
                            $context['previewTable'] = $tables['main'];
                        }
                        if (!empty($tables['additional'])) {
                            $context['previewDescription'] = $tables['additional'];
                        }
                    }

                    if (!empty($parameters)) {
                        $tables = $md->getTables($parameters);
                        if (!empty($tables['main'])) {
                            $context['mainTable'] = $tables['main'];
                        }
                        if (!empty($tables['additional'])) {
                            $context['additionalTable'] = $tables['additional'];
                        }
                    }

                    if (!empty($action['deprecated']) && $action['deprecated'] == true) {
                        $action['summary'] .= ' (Deprecated)';
                    }

                    if (!empty($action['responses'])) {
                        $action['path'] = $actionPath;
                        $context['fullDescription'] = $md->getActionDescription($action);
                    }
                    $data[] = [
                        'template' => $template,
                        'file'     => $file,
                        'context'  => array_merge($action, $context)
                    ];
                }
            }
        }
        return $data;
    }

    public function formatExample($string)
    {
        $example = null;
        $path = $this->getPathToGenerator();
        $path .= '/templates/snippet.json';
        if (is_file($path)) {
            $snippets = json_decode(file_get_contents($path), true);
            if (!empty($snippets[$string]['content'])) {
                $example = PHP_EOL . '    ' . PHP_EOL . "Пример:" . PHP_EOL;
                $example .= '```json' . PHP_EOL;
                $example .= json_encode($snippets[$string]['content'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
                $example .= PHP_EOL . '```' . PHP_EOL;
            }
        }
        return $example;
    }

    public function formatString($string)
    {
        preg_match_all("/<!--\[%swg_snippet.(.+)\(\)%\]-->/", $string, $matches);
        if (!empty($matches)) {
            foreach ($matches[0] as $key => $match) {
                $path = $this->getPathToGenerator();
                $path .= '/templates/snippet.json';
                if (is_file($path)) {
                    $snippets = json_decode(file_get_contents($path), true);
                    if (!empty($snippets[$matches[1][$key]])) {
                        $snippet = $snippets[$matches[1][$key]];
                        if (!empty($snippet['type'])) {
                            switch ($snippet['type']) {
                                case 'replace':
                                    $replace = $snippet['content'];
                                    $string = str_replace($match, $replace, $string);
                                    break;
                            }
                        }
                    }
                }
            }
        }
        return $string;
    }

    public function getControllers()
    {
        $controllers = [];
        $properties = $this->getProperties();
        $docDirectory = $this->getPathToDocs();

        foreach ($properties['definitions'] as $key => $definition) {
            if (!empty($definition['title']) && $definition['title'] == 'Controller') {
                $path = self::getDirByPath($docDirectory . '/controllers/' . $key);
                $template = $this->getPathToTemplate('controller');
                if (!empty($template) && !empty($path)) {
                    $file = $path . '/README.md';
                    $controllers[] = [
                        'template' => $template,
                        'file'     => $file,
                        'vars'     => [
                            'tag' => trim($definition['properties']['tag']['default'])
                        ]
                    ];
                }
            }
        }
        return $controllers;
    }

    public function getListModels()
    {
        $docDirectory = $this->getPathToDocs();
        $models = $this->getModels();
        $context = [];
        if (!empty($models)) {
            $others = [];
            foreach ($models as $model) {
                $xmlName = trim($model['context']['xml']['name']);
                $title = (!empty($model['context']['title'])) ? $model['context']['title'] : trim($xmlName);

                $element = [
                    'file'  => '../' . $model['context']['pathToModel'] . DIRECTORY_SEPARATOR . 'README.md',
                    'title' => $title
                ];
                if (!empty($model['context']['x-namespace'])) {
                    $namespaces = $model['context']['x-namespace'];
                    $namespaces = explode('/', $namespaces);
                    $currentData = &$data['models'];
                    foreach ($namespaces as $namespace) {
                        if (empty($currentData[$namespace])) {
                            $currentData[$namespace] = [];
                        }
                        $currentData = &$currentData[$namespace];
                    }

                    if (empty($currentData['models'])) {
                        $currentData['models'] = [];

                    }
                    $currentData['models'][] = $element;
                } else {
                    $others[] = $element;
                }
            }

            if (!empty($others)) {
                $namespace = 'Прочее';
                $context['models'][$namespace]['models'] = $others;
            }
        }

        $file = $docDirectory . '/models/README.md';
        $data = [
            'template' => 'models.twig',
            'file'     => $file,
            'context'  => $context
        ];
        return $data;
    }

    public function getModels()
    {
        $properties = $this->getProperties();
        $data = [];
        $docDirectory = $this->getPathToDocs();

        foreach ($properties['definitions'] as $key => $definition) {
            if (!empty($definition['xml']['name'])) {
                $pathToModel = 'models' . DIRECTORY_SEPARATOR . $key;
                $pathToModel = $this->getDirByPath($pathToModel);
                $path = self::getDirByPath($docDirectory . DIRECTORY_SEPARATOR . $pathToModel);
                if (!empty($path)) {
                    $file = $path . '/README.md';
                    $md = $this->getMarkdown();
                    $md->setActionPath('../..');
                    $context = [];
                    if (empty($definition['title'])) {
                        $definition['title'] = $definition['xml']['name'];
                    }

                    if (!empty($definition['properties'])) {
                        if (!empty($definition['x-deprecated'])) {
                            $definition['title'] .= ' (Deprecated)';
                        }
                        foreach ($definition['properties'] as $name => $property) {
                            $definition['properties'][$name]['name'] = $name;
                        }
                        $options = [];

                        if (!empty($definition['required'])) {
                            $options['required'] = $definition['required'];
                        }

                        $tables = $md->getTables($definition['properties'], $options);
                        if (!empty($tables['main'])) {
                            $context['main'] = $tables['main'];
                        }

                        if (!empty($tables['additional'])) {
                            $context['additional'] = $tables['additional'];
                        }
                    }
                    $schema = $this->getSchema();
                    $example = $schema->getExampleByModel($definition);
                    if (!empty($example)) {
                        $context['example'] = $md->getJson($example);
                    }
                    $context['pathToModel'] = $pathToModel;
                    $context['docPath'] = preg_replace("/([^\/]+)/", "..", $pathToModel);
                    $data[] = [
                        'template' => 'model.twig',
                        'file'     => $file,
                        'context'  => array_merge($definition, $context)
                    ];
                }

            }
        }
        return $data;
    }
}
